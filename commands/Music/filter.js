const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
const {format} = require("../../handlers/functions.js");
const filters = [
    "clear",
    "lowbass" ,
    "bassboost",
    "purebass" ,
    "8D",
    "vaporwave",
    "nightcore",
    "phaser",
    "tremolo",
    "vibrato",
    "reverse",
    "treble",
    "normalizer",
    "surrounding",
    "pulsator",
    "subboost",
    "karaoke",
    "flanger",
    "gate",
    "haas",
    "mcompand"
]
module.exports = {
    name: "filter",
    category: "Music",
    aliases: ["fil"],
    cooldown: 2,
    usage: "filter <Filtertype>" + `\n`+ "FILTERS: \n(clear , lowbass , bassboost ,purebass ,8D ,vaporwave ,nightcore , phase , tremolo , vibrato , reverse , treble , normalizer , surroundingpulsator , subboost , karaoke , flanger , gate , haas , mcompand)>",
    description: "Changes the audio filter" ,
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)

       );

        
       

        if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
        return message.channel.send(new MessageEmbed()
             .setColor(ee.wrongcolor)
             .setFooter(ee.footertext, ee.footericon)
             .setTitle(`❌ ERROR | Please join **my** voice channel`)
             .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
  
         );



     
         if(!args[0])
         return message.channel.send(new MessageEmbed()
              .setColor(ee.wrongcolor)
              .setFooter(ee.footertext, ee.footericon)
              .setTitle(`❌ ERROR | Please add a filter type`)
              .setDescription(`Usage: \`${prefix}filter <Filtertype>\`\nExample: \`${prefix}filter bassboost\``)
   
          );


       if(!filters.join(" ").toLowerCase().split(" ").includes(args[0].toLowerCase()))
      return message.channel.send(new MessageEmbed()
       .setColor(ee.wrongcolor)
      .setFooter(ee.footertext, ee.footericon)
      .setTitle(`❌ ERROR | Not a valid filtertype`)
     .setDescription(`Usage: \`${prefix}filter <Filtertype>\`\nFilter types:\n>\`${filters.join("`, `")}\``.substr(0, 2048))
     );
     
          client.distube.setFilter(message,args[0]);

          message.channel.send(new MessageEmbed()
          .setColor(ee.color)
         .setFooter(ee.footertext, ee.footericon)
         .setTitle(`✅ Successfully set filter to: \ ${args[0]}\` `)
          ).then(msg=>msg.delete({timeout:4000}).catch(e=>console.log(e.message)))

    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
