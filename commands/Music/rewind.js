const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
const {format} = require("../../handlers/functions.js");
module.exports = {
    name: "rewind",
    category: "Music",
    aliases: ["rwd"],
    cooldown: 2,
    usage: "rewind <time> in seconds",
    description: "Rewind by ab amount of time <seconds> in the current song",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)

       );

        
     if(!args[0])
     return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | You didn't provide a forward duration`)
            .setDescription(`Usage: \`${prefix}seek 10\``));


         
         let queue = client.distube.getQueue(message);
          let seektime = queue.currentTime - Number(args[0]*1000);
          if(seektime < 0)
          seektime = 0;
        if(seektime >= queue.songs[0].duration * 1000 - queue.currentTime)
         seektime = 0;

         client.distube.seek(message , seektime);
       
       message.channel.send(new MessageEmbed()
       .setColor(ee.color)
      .setFooter(ee.footertext, ee.footericon)
      .setTitle(`⏪ Rewinded by \`${args[0]} seconds\` to: ${format(seektime)}`)
       ).then(msg=>msg.delete({timeout:4000}).catch(e=>console.log(e.message)))
          

    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
