const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
const {functions} = ("../../handlers/functions.js");
module.exports = {
    name: "queue",
    category: "Music",
    aliases: ["q"],
    cooldown: 2,
    usage: "queue",
    description: "Shows the current Queue",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
       );
         
       let queue = client.distube.getQueue(message);
       if(!queue)
      return message.channel.send(new MessageEmbed()
       .setColor(ee.wrongcolor)
      .setFooter(ee.footertext, ee.footericon)
      .setTitle(`❌ ERROR | There is nothing playing`)
      .setDescription(`The Queue is empty`)
       );

    
       
        let embed = new MessageEmbed()
            .setColor(ee.color)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`Current Queue for: ${message.guild.name}`)
            let counter = 0;
            for (let i =0 ; i< queue.songs.length; i+=20){
              if(counter>=15)break;
              let k = queue.songs;
              let songs = k.slice(i, i +20);
              message.channel.send(embed.setDescription(songs.map((song, index)=> `**${index +1 +counter *20 }** [${song.name}](${song.url})- ${format(song.Duration)}`)))
              counter++;
            }
       

      
    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
