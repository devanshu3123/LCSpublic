const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
module.exports = {
    name: "loop",
    category: "Music",
    aliases: ["l","repeat"],
    cooldown: 2,
    usage: "loop <0/1/2> {[0-No loop] [1-Loop song] , [2-Loop Queue]}",
    description: "Changes loop from off/song/queue",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
       );
         
       

       if(!args[0]){
       return message.channel.send(new MessageEmbed()
       .setColor(ee.wrongcolor)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle(`❌ ERROR | You didn't provide a loop method`)
       .setDescription(`Usage: \`${prefix}loop <0/1/2> {[0-No loop] [1-Loop song] , [2-Loop Queue]} \``)
       );
       }
       
        let loopstate = args[0].toString();
        if (loopstate.toLowerCase()=== "song") loopstate ="1";      
        if (loopstate.toLowerCase()=== "queue") loopstate ="2";
        if (loopstate.toLowerCase()=== "off") loopstate ="0";
        if (loopstate.toLowerCase()=== "track") loopstate ="1";
        if (loopstate.toLowerCase()=== "q") loopstate ="2";
        if (loopstate.toLowerCase()=== "qu") loopstate ="2";
        if (loopstate.toLowerCase()=== "disable") loopstate ="0";
        loopstate = Number(loopstate);
        loopstates = {
            "0" : "off", 
            "1" : "song",
            "2" : "queue"
        }

        if(0 <= loopstate && loopstate <= 2){
            client.distube.setRepeatMode(message, parseInt(loopstate));
           message.channel.send(new MessageEmbed()
             .setColor(ee.color)
             .setFooter(ee.footertext,ee.footericon)
             .setTitle(`🔁 Changed repeat mode to: \`${loopstates[loopstate]}\``)
            ).then(msg=>msg.delete({timeout:4000}).catch(e=>console.log(e.message)));
        }

        else 
        { return message.channel.send(new MessageEmbed()
        .setColor(ee.wrongcolor)
        .setFooter(ee.footertext, ee.footericon)
        .setTitle(`❌ ERROR | You didn't provide a loop method`)
        .setDescription(`Usage: \`${prefix}loop <0/1/2> {[0-No loop] [1-Loop song] , [2-Loop Queue]}\``)
        
    );
        }

    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
