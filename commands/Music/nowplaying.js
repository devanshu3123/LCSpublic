


const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
const {format, createBar} = require("../../handlers/functions.js");
module.exports = {
    name: "nowplaying",
    category: "Music",
    aliases: ["np"],
    cooldown: 2,
    usage: "nowplaying",
    description: "Shows the Track information",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)

       );

        
       let queue = client.distube.getQueue(message);
       let track = queue.songs[0];
       message.channel.send(new MessageEmbed()
       .setColor(ee.color)
      .setFooter(ee.footertext, ee.footericon)
      .setTitle(`Now playing :notes: ${track.name}`.substr(0,256))
      .setURL(track.url)
      .setThumbnail(track.thumbnail)
      .addField("Views ",`▶${track.views}`,true)
      .addField("Likes 👍", `${track.likes}`,true)
      .addField("Disikes 👎",  `${track.dislikes}`,true)
      .addField("Duration ⌛", createBar(queue.currentTime))
       ).then(msg=>msg.delete({timeout:5000}).catch(e=>console.log(e.message)))
          

    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
