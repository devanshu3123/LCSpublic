const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
module.exports = {
    name: "search",
    category: "Music",
    aliases: ["find"],
    cooldown: 2,
    usage:"search track name",
    description: "Plays a song",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );


     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
       );

      if(!args[0])
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | You didn't provide a searchterm`)
            .setDescription(`Usage: \`${prefix}search <Track name or URL>\``)
        );

          let result = await client.distube.search(args.join(" "));
          let searchresult = "";
          for (let i = 0; i<10;i++){
              try
              {
                  searchresult += `**${i+1}.**[${result[i].name}]($(${result[i].url})- \`${result[i].formattedDuration}\`\n\)`
              }
              catch{
                  searchresult = "\n";
              }

            }
               message.channel.send(new MessageEmbed()
               .setColor(ee.color)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`Searchresult for:${args.join(" ")}`.substr(0,256))
            .setDescription(searchresult.substr(0, 2048))
               ).then(msg=>{
                   msg.channel.awaitMessages(m => m.author.id === message.author.id, {max:1,time:60000, errors: ["time"]}).then(collected => {
                       let userinput = collected.first().content;
                       if(Number(userinput) <= 0 && Number(userinput)>10){
                           message.reply("Not a valid number , so playing First track")
                           userinput = 0;
                       }
                       client.distube.play(message,result[userinput-1].url);
                   }).catch (e=> {
                    console.log(String(e.stack).bgRed)
                    return message.channel.send(new MessageEmbed()
                        .setColor(ee.wrongcolor)
                        .setFooter(ee.footertext, ee.footericon)
                        .setTitle(`❌ ERROR | An error occurred`)
                        .setDescription(`\`\`\`${e.stack}\`\`\``)
                    );
                })
               })

    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
