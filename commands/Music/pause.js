const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
module.exports = {
    name: "pause",
    category: "Music",
    aliases: [""],
    cooldown: 2,
    usage: "pause",
    description: "Pause the current song",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
       );
         
        
     if(client.distube.isPaused(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Cannot pause the song`)
            .setDescription(`The song is already paused`)
     )

       message.channel.send(new MessageEmbed()
       .setColor(ee.wrongcolor)
      .setFooter(ee.footertext, ee.footericon)
      .setTitle("⏸ Paused the song")
       ).then(msg=>msg.delete({timeout:4000}).catch(e=>console.log(e.message)))
        
       client.distube.pause(message);
      

      
    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
