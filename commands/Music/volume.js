const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
module.exports = {
    name: "volume",
    category: "Music",
    aliases: ["vol",],
    cooldown: 2,
    usage: "volume <0-150>",
    description: "Changes volume of the Queue",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );

      
     if(!client.distube.getQueue(message))
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | There is nothing playing`)
            .setDescription(`The Queue is empty`)
     );
     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
       );
         
       

       if(!args[0]){
       return message.channel.send(new MessageEmbed()
       .setColor(ee.wrongcolor)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle(`Queue volume`)
       .setDescription(`Current volume: \`${client.distube.getQueue(message).volume}%\`\nUsage: \`${prefix}volume<0-150>\``)
       );
       }





       if(!(0<= Number(args[0]) && Number(args[0]) <=150 ))
       return message.channel.send(new MessageEmbed()
       .setColor(ee.wrongcolor)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle(`❌ ERROR | Volume out of range`)
       .setDescription(`Usage: \`${prefix}volume <0-150>\``)
       );

       client.distube.setVolume(message, Number(args[0]));
       return message.channel.send(new MessageEmbed()
       .setColor(ee.color)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle(`🔊 Changed the volume to: \`${args[0]}%\``)
       .setDescription(`Usage: \`${prefix}volume <0-150>\``)
       );


       
        

    

    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
