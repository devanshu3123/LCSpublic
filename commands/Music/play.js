const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
const {getTracks , getPreview} = require("spotify-url-info");
module.exports = {
    name: "play",
    category: "Music",
    aliases: ["p"],
    cooldown: 2,
    usage: "play URL or track name",
    description: "Plays a song",
    run: async (client, message, args, user, text, prefix) => {
    try{
      const {channel} = message.member.voice;
      
     if(!channel)
     return message.channel.send(new MessageEmbed()
      
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | Please join a voice channel first`)
     );


     
     if(client.distube.isPlaying(message)&& channel.id !=message.guild.me.voice.channel.id)
      return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${message.guild.me.voice.channel.name}\``)
       );

      if(!args[0])
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | You didn't provide a Track name`)
            .setDescription(`Usage: \`${prefix}play <Track name or URL>\``)
        );
      message.channel.send(new MessageEmbed()
             .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle("Searching song")
            .setDescription(`\`\`\`fix\n${text}\`\`\``)
      ).then(msg=>msg.delete({timeout: 3000}).catch(e=>console.log(e,message)));
          
      if(args.join(" ").toLowerCase().includes("spotify")&& args.join(" ").toLowerCase().includes("track"))
      {
        getPreview(args.join(" ")).then((result => {
          client.distube.play(message, result.title);
        }))

      }else if (args.join(" ").toLowerCase().includes("spotify")&& args.join(" ").toLowerCase().includes("playlist"))
      {
        getTracks(args.join(" ")).then((result => {
          for(const playList of result)
          client.distube.play(message, playList.name);
        }))
      }

      else { 
      client.distube.play(message, text);
      }
    } catch (e) {
        console.log(String(e.stack).bgRed)
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | An error occurred`)
            .setDescription(`\`\`\`${e.stack}\`\`\``)
        );
    }
  }
}
