const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
module.exports = {
    name: "poll",
    category: "Administration",
    aliases: [""],
    cooldown: 2,
    usage: "poll + <Your text>",
    description: "Creates a poll",
    run: async (client, message, args, user, text, prefix) => {
        try{
            if(!args[0])
            return message.channel.send(new MessageEmbed()
                .setColor(ee.wrongcolor)
                .setFooter(ee.footertext, ee.footericon)
                .setTitle("❌ ERROR | No poll text")
                .setDescription(`Usage: \`${prefix}poll <poll text>\``)
            );
          message.channel.send(new MessageEmbed()
                 .setColor(ee.wrongcolor)
                .setFooter(ee.footertext, ee.footericon)
                .setTitle("📌 Poll")
            .setDescription("**" + text + "**")
          ).then(messageReaction => {
            messageReaction.react("👍");
            messageReaction.react("👎");
            (msg=>msg.delete({timeout: 3000}).catch(e=>console.log(e,message)));
        });

              
        } catch (e) {
            console.log(String(e.stack).bgRed)
            return message.channel.send(new MessageEmbed()
                .setColor(ee.wrongcolor)
                .setFooter(ee.footertext, ee.footericon)
                .setTitle(`❌ ERROR | An error occurred`)
                .setDescription(`\`\`\`${e.stack}\`\`\``)
            );
        }
      }
    }
    
        
                  
