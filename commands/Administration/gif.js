const fetch = require("node-fetch");
const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");

module.exports = {
    name: "gif",
    category: "Administration",
    aliases: [""],
    cooldown: 2,
    usage: "gif + searchtems",
    description: "Sends a gif",
    run: async (client, message, args, user, text, prefix) => {
        try{
            if(!args[0]){
                return message.channel.send(new MessageEmbed()
                .setColor(ee.wrongcolor)
                .setFooter(ee.footertext, ee.footericon)
                .setTitle("No search term found")
                .setDescription(`Usage: \`${prefix}gif <Search term>\``)
            );
    };
    
    
    let url = `https://g.tenor.com/v1/search?q=${text}&key=${process.env.TENORKEY}&limit=8`
      let response = await fetch(url);
      let json = await response.json();
      
      const index = Math.floor(Math.random() * json.results.length);
      message.channel.send(json.results[index].url);
    
        }
 catch (e) {
    console.log(String(e.stack).bgRed)
    return message.channel.send(new MessageEmbed()
        .setColor(ee.wrongcolor)
        .setFooter(ee.footertext, ee.footericon)
        .setTitle(`❌ ERROR | An error occurred`)
        .setDescription(`\`\`\`${e.stack}\`\`\``)
    );
}
}
}
