const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
module.exports = {
    name: "tweet",
    category: "Administration",
    aliases: ["tw"],
    cooldown: 2,
    usage: "tweet + <user mention>",
    description: "Creates a tweet",
    run: async (client, message, args, user, text, prefix) => {
        try{

            let date_ob = new Date();

            // current date
            // adjust 0 before single digit date
            let date = ("0" + date_ob.getDate()).slice(-2);
            
            // current month
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            
            // current year
            let year = date_ob.getFullYear();
            
            // current hours
            let hours = date_ob.getHours();
            
            // current minutes
            let minutes = date_ob.getMinutes();
            
            // current seconds
            let seconds = date_ob.getSeconds();


            
            const member =  message.member;
            if(!args[0])
            return message.channel.send(new MessageEmbed()
                .setColor(`#008BFF`)
                .setFooter(hours + ":" + minutes + "   "+  month + "-" + date + "-" + year )
                .setTitle(`${member.user.tag} ☑ `).setDescription("We used to be good friends from November 2020, & it never occurred that he'll breach all his limitations & say something like this, although, he knows personal details yet this needs to be put out now & here I am mentioning his discord acc " + text)
            );
          message.channel.send(new MessageEmbed()
                 .setColor(`#008BFF`)
                .setFooter(hours + ":" + minutes + "   "+  month + "-" + date + "-" + year)
                .setTitle(`${member.user.tag} ☑`)
            .setDescription("We used to be good friends from November 2020, & it never occurred that he'll breach all his limitations & say something like this, although, he knows personal details yet this needs to be put out now & here I am mentioning his discord acc " +  text)
          )
    ;

              
        } catch (e) {
            console.log(String(e.stack).bgRed)
            return message.channel.send(new MessageEmbed()
                .setColor(ee.wrongcolor)
                .setFooter(ee.footertext, ee.footericon)
                .setTitle(`❌ ERROR | An error occurred`)
                .setDescription(`\`\`\`${e.stack}\`\`\``)
            );
        }
      }
    }