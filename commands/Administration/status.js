const { MessageEmbed } = require("discord.js");
const config = require("../../botconfig/config.json");
const ee = require("../../botconfig/embed.json");
module.exports = {
    name: "status",
    category: "Administration",
    aliases: [""],
    cooldown: 2,
    usage: "status + <Bot status>",
    description: "Changes the status of Bot",
    run: async (client, message, args, user, text, prefix) => {
        try{ 

            
           
            if(!message.member.hasPermission('ADMINISTRATOR'))
                return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`Status cannot be changed`)
            .setDescription(`You do not have admin perms`));
            
            if(!args[0])
        return message.channel.send(new MessageEmbed()
            .setColor(ee.wrongcolor)
            .setFooter(ee.footertext, ee.footericon)
            .setTitle(`❌ ERROR | You didn't provided a Text for status`)
            .setDescription(`Usage: \`${prefix}status <Text for status>\``));


        message.channel.send(new MessageEmbed()
        .setColor(ee.wrongcolor)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle("Bot status")
        .setDescription("Bot status set to " + "**" + text + "**")).then(client.user.setPresence({
            activity:{
                name:text,
                type:0,
            }
        }));

        }
        catch (e) {
            console.log(String(e.stack).bgRed)
            return message.channel.send(new MessageEmbed()
                .setColor(ee.wrongcolor)
                .setFooter(ee.footertext, ee.footericon)
                .setTitle(`❌ ERROR | An error occurred`)
                .setDescription(`\`\`\`${e.stack}\`\`\``)
            );
        }
      }
    }
