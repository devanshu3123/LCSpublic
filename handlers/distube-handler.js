
const Distube = require("distube");
const { MessageEmbed } = require("discord.js");
const config = require("../botconfig/config.json");
const ee = require("../botconfig/embed.json");
const {format, delay} = require("../handlers/functions");
const { on } = require("distube");

module.exports= (client) => {
    
    client.distube = new Distube(client,{
        searchSongs:false,
        emitNewSongOnly:false,
        highWaterMark:1024*1024*64,
        leaveOnEmpty:false,
        leaveOnStop:false,
        leaveOnFinish:false,
        //youtubeCookie
        youtubeDL:true,
        updateYouTubeDL:true,
        customFilters : {
            "clear": "dynaudnorm=f=200",
            "lowbass" : "bass=g=6,dynaudnorm=f=200",
            "bassboost": "bass=g=20,dynaudnorm=f=200",
            "purebass" : "bass=g=20,dynaudnorm=f=200,asubboost,apulsator=hz=0.08",
            "8D": "apulsator=hz=0.08",
            "vaporwave": "aresample=48000,asetrate=48000*0.8",
            "nightcore": "aresample=48000,asetrate=48000*1.25",
            "phaser": "aphaser=in_gain=0.4",
            "tremolo": "tremolo",
            "vibrato": "vibrato=f=6.5",
            "reverse": "areverse",
            "treble": "treble=g=5",
            "normalizer": "dynaudnorm=f=200",
            "surrounding": "surround",
            "pulsator": "apulsator=hz=1",
            "subboost": "asubboost",
            "karaoke": "stereotools=mlev=0.03",
            "flanger": "flanger",
            "gate": "agate",
            "haas": "haas",
            "mcompand": "mcompand"
            }
            


    })
// Queue status template
const status = (queue) => `Volume: \`${queue.volume}%\` | Filter: \`${queue.filter || "Off"}\` | Loop: \`${queue.repeatMode ? queue.repeatMode == 2 ? "All Queue" : "This Song" : "Off"}\` | Autoplay: \`${queue.autoplay ? "On" : "Off"}\``;

// DisTube event listeners, more in the documentation page
client.distube
    .on("playSong", (message, queue, song) => message.channel.send(new MessageEmbed()
             .setTitle("Playing now : "+ song.name)
             .setURL(song.url)
             .addField("Duration" , `\`${song.formattedDuration}\``)
             .setColor(ee.color)
             .setThumbnail(song.thumbnail)
            .setFooter(`Requested by :${song.user.tag}`, song.user.displayAvatarURL({dynamic: true})
            
            )).then( async msg => {
               let emojiarray= ["⏭","⏹","⏯","⏪","⏩","🔉","🔊"];
               for(const emoji of emojiarray)
               await msg.react(emoji)

             var filter = (reaction, user) => 
                 emojiarray.includes(reaction.emoji.name) && user.id !== message.client.user.id
                
                   
                var collector = await msg.createReactionCollector(filter, {time: song.duration > 0 ? song.duration * 1000 : 180000})
                 collector.on("collect", async(reaction, user) => {
                     console.log("QUEUE")
                    if (!queue) return ; 
                     const member= reaction.message.guild.member(user);
                     reaction.users.remove(user);
                     if(!member.voice.channel)
                     return message.channel.send(new MessageEmbed()
                     .setColor(ee.color)
                     .setFooter(ee.footertext,ee.footericon)
                       .setTitle(`❌ ERROR | Please join a voice channel first`)
                 ) 

                 if(member.voice.channel.id !== member.guild.voice.channel.id)
             return message.channel.send(new MessageEmbed()
           .setColor(ee.wrongcolor)
           .setFooter(ee.footertext, ee.footericon)
           .setTitle(`❌ ERROR | Please join **my** voice channel`)
           .setDescription(`Channelname: ${member.guild.voice.channel.name}\``)
       )
        
          switch(reaction.emoji.name){
              case "⏭":
                  message.channel.send(new MessageEmbed() 
                  .setColor(ee.color)
                  .setFooter(ee.footertext, ee.footericon)
                  .setTitle(`⏭Skipped the track`)
                  ).then(msg => msg.delete({timeout:3000}))
                client.distube.skip(reaction.message)

                break ; 

                case "⏹":
                    message.channel.send(new MessageEmbed() 
                    .setColor(ee.color)
                    .setFooter(ee.footertext, ee.footericon)
                    .setTitle(`⏹Stopped the track`)
                    ).then(msg => msg.delete({timeout:3000}))
                  client.distube.stop(reaction.message)
  
                  break ; 

                  case "⏯":




                    if(client.distube.isPaused(message)){
                        client.distube.resume(message);
                        await delay(100);
                        client.distube.pause(message);
                        await delay(100);
                        client.distube.resume(message);
                    return message.channel.send(new MessageEmbed()
                     
                           .setColor(ee.color)
                           .setFooter(ee.footertext, ee.footericon)
                           .setTitle("▶Resumed the song")
                    ).then(msg =>msg.delete({timeout:3000}).catch(e=>console.log(e.message)))
                    }
                     message.channel.send(new MessageEmbed()
                     
                           .setColor(ee.color)
                           .setFooter(ee.footertext, ee.footericon)
                           .setTitle(`⏸Paused the song`)
                           ).then(msg => msg.delete({timeout:3000}).catch(e=>console.log(e.message)))
                  client.distube.pause(message);
                           break ; 



                           case "⏪":


                            let seektime = queue.currentTime - 15*1000;
                            if(seektime < 0)
                            seektime = 0;
                          if(seektime >= queue.songs[0].duration * 1000 - queue.currentTime)
                           seektime = 0;
                  
                           client.distube.seek(message , seektime);
                         
                         message.channel.send(new MessageEmbed()
                         .setColor(ee.color)
                        .setFooter(ee.footertext, ee.footericon)
                        .setTitle(`⏪ Rewinded by \`15 seconds\` to: ${format(seektime)}`)
                         ).then(msg=>msg.delete({timeout:4000}).catch(e=>console.log(e.message)))
           
                        break;
                      case "⏩":
                                              
         
          let seektime2 = queue.currentTime + 15 * 1000;
         if(seektime2<0)
         seektime2 = queue.songs[0].duration * 1000;
       if(seektime2 >= queue.songs[0].duration * 1000)
        seektime2 = queue.songs[0].duration*1000-1000;
        client.distube.seek(reaction.message , seektime2);
        message.channel.send(new MessageEmbed()
       .setColor(ee.color)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle(`⏩Forwarded for \`15 seconds\` to: ${format(seektime2)}`)
       ).then(msg => msg.delete({timeout:3000}).catch(e=>console.log(e.message)));
                        break ;





                      case "🔉":
                             
        client.distube.setVolume(message, queue.volume-10);
       if (queue.volume <10)client.distube.setVolume(message ,0);
       return message.channel.send(new MessageEmbed()
       .setColor(ee.color)
       .setFooter(ee.footertext, ee.footericon)
       .setTitle(`🔉 Reduced the volume for \`10%\` to: \`${queue.volume}%\``)
       ).then(msg => msg.delete({timeout:3000}).catch(e=>console.log(e.message)));
        break;
        break;
                        case "🔊":
                            client.distube.setVolume(reaction.message,queue.volume+10);
                        if (queue.volume <10)client.distube.setVolume(message ,150);
                        return message.channel.send(new MessageEmbed()
                        .setColor(ee.color)
                        .setFooter(ee.footertext, ee.footericon)
                        .setTitle(`🔊 Increased the volume for \`10%\` to: \`${queue.volume}%\``)
                        ).then(msg => msg.delete({timeout:3000}).catch(e=>console.log(e.message)));
                         break;
                        
                         break; 
                  
          }   
          
          collector.on("end",()=>{
              try{
                  msg.delete()
              }
              catch{}
          })

       })
      })
      )      







       .on("addSong", (message, queue, song) => message.channel.send(new MessageEmbed()
       .setTitle("Song added to Queue " + song.name)
       .setURL(song.url)
        .addField("Duration" , `\`${song.formattedDuration}\``)
        .setColor(ee.color)
        .setThumbnail(song.thumbnail)
        .setFooter(`Requested by :${song.user.tag}`, song.user.displayAvatarURL({dynamic: true}))
        .addField(`${queue.songs.length} songs in Queue`, `Duration: ${queue.formattedDuration}`)
))

              
    .on("playList", (message, queue, playlist, song) => message.channel.send(new MessageEmbed()
    .setTitle("Playlist added to Queue " + playlist.name)
    .addField("Now playing"+ song.name)
   .setURL(playlist.url)
    .addField("Duration" , `\`${song.formattedDuration}\``)
    .setColor(ee.color)
    .setThumbnail(song.thumbnail)
    .setFooter(`Requested by :${playlist.user.tag}`, playlist.user.displayAvatarURL({dynamic: true}))
    .addField(`${queue.songs.length} songs in Queue`, `Duration: ${(queue.formattedDuration)}`)
    )) 
             
 .on("addList", (message, queue, playlist) => message.channel.send(new MessageEmbed()
    .setTitle("Playlist playing: " + playlist.name)
    .setURL(playlist.url)
    .addField("Duration" , `\`${song.formattedDuration}\``)
    .setColor(ee.color)
    .setThumbnail(song.thumbnail)
    .setFooter(`Requested by :${playlist.user.tag}`, playlist.user.displayAvatarURL({dynamic: true}))
    .addField(`${queue.songs.length} songs in Queue`, `Duration: ${(queue.formattedDuration)}`)
    ))






            



    
    // DisTubeOptions.searchSongs = true
    .on("searchResult", (message, result) => {
        let i = 0;
        message.channel.send(`**Choose an option from below**\n${result.map(song => `**${++i}**. ${song.name} - \`${song.formattedDuration}\``).join("\n")}\n*Enter anything else or wait 60 seconds to cancel*`);
    })
    // DisTubeOptions.searchSongs = true
    .on("searchCancel", (message) => message.channel.send(`Searching canceled`))
    .on("error", (message, e) => {
        console.error(e)
        message.channel.send("An error encountered: " + e);
    })
    .on("initQueue", queue=> {
        queue.autoplay = false;
        queue.volume = 75;
        queue.filter = "lowbass";
    })

}